import React, { Component } from 'react';
import Modal from './Modal';
import './ModalButton.css';

class ModalButton extends Component {
  state = {
    isModalShow: false
  };

  hideModal = () => this.setState({ isModalShow: false });

  showModal = () => this.setState({ isModalShow: true });

  render() {
    const { state: {isModalShow}, hideModal, showModal } = this;
    
    return (
        <div>
          {
            isModalShow ? 
            <Modal hideModal={hideModal}/>: 
            <button onClick={showModal}>Show modal!</button>
          }    
        </div>
    );
  }
}
  

export default ModalButton;


