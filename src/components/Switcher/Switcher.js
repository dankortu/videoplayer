import React, { Fragment, Component } from 'react';
import './Switcher.css';


class Switcher extends Component {
  state = {
    selectedChild: 0
  };

  handleChangeChild = (e) =>{
    let id = e.target.dataset.id;
    this.setState({selectedChild: id});
  };

  render() {
    const { 
      state: { selectedChild }, 
      props: { children },
      handleChangeChild  
    } = this;

    return (
      <Fragment>
        <ul className="component-list">

          {React.Children.map(children, (item, index)=>{
            const {name, displayName} = item.type;
            const nameOfChild = displayName || name;

            return (
              <li className="component-list__name" data-id={index} onClick={handleChangeChild}>
                {nameOfChild}
              </li>
            )
          })}
        </ul>

        {React.Children.map(children, (item, index)=>{

          if (index == selectedChild) {
            return item;
          }
        })}
      </Fragment>
    );
  }
}

export default Switcher;
