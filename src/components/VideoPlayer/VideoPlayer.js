import React from 'react';
import videoFile from './Video.mp4';
import './VideoPlayer.css';

const VideoPlayer = () => {
  const videoRef = React.createRef();
  
  const playVideo = () => {
    videoRef.current.play();
  };
  
  const stopVideo = () => {
    videoRef.current.pause();
  };

  return (
    <div className="video-wrapper">
      <video className="video-player" ref={videoRef} controls>
        <source className="video-player__source" src={videoFile}/>
      </video>
      <div className="controls-panel">
        <button onClick={playVideo}>Start</button>
        <button onClick={stopVideo}>Stop</button>
      </div>
    </div>
  );
};

export default VideoPlayer;
