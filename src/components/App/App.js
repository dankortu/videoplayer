import React from 'react';
import './App.css';
import VideoPlayer from '../VideoPlayer/VideoPlayer';
import Switcher from 'components/Switcher';
import CardNumberHolder from 'components/CardNumberHolder';
import ModalButton from 'components/ModalButton';

const App = () => {
  return (
    <Switcher>
      <VideoPlayer />
      <CardNumberHolder />
      <ModalButton />
    </Switcher>
  );
};

export default App;
