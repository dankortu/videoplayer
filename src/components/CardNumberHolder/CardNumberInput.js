import React, { Component } from 'react';

class CardNumberInput extends Component {
  state = {
    number: this.props.cardNumber
  };

  format = value => {
    let numbersStr = String(value).replace(/[^0-9]/g, '');
    return String(numbersStr).replace(/.{4}/g, match => match + ' ');
  };

  normalize = str => {
   return str.replace(/\s/g, '')
  };

  componentDidMount() {
    const { format } = this;
    const { cardNumber } = this.props;

    this.setState({ number: format(cardNumber) });
  }

  componentWillReceiveProps = nextProps => {
    const { format } = this;
    const { cardNumber } = nextProps;

    this.setState({ number: format(cardNumber) });
  };

  handleOnChange = (event) => {
    const { 
      props: { onChange },
      normalize
    } = this;

    onChange(normalize(event.target.value));
  };

  render() {
    const {
      state: { number }, 
      handleOnChange
    } = this;

    return <input value={number} onChange={handleOnChange}/>;
  }
}


export default CardNumberInput;