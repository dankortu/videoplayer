import React, { Component } from 'react';
import CardNumberInput from './CardNumberInput';

class CardNumberHolder extends Component {
  state = {
    cardNumber: ''
  };

  handleChange = (value) => {
    this.setState({
       cardNumber: value
    })
  };

  render() {
    const { state:{cardNumber}, handleChange } = this;
    return <CardNumberInput cardNumber={cardNumber} onChange={handleChange}/>;
  }
}

export default CardNumberHolder;
